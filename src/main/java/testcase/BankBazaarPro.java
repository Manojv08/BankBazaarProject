package testcase;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.HomePage;
import wdMethods.ProjectMethods;

public class BankBazaarPro extends ProjectMethods{
	
	@BeforeClass
	public void setData() {
		testCaseName = "BankBazaarProject";
		testCaseDescription ="Scheme and Amounts";
		category = "Smoke";
		author= "Manoj";
	}
	
	@Test
	public void getSchemeAndAmounts() throws InterruptedException {
		new HomePage()
		.mouseOverOnInvestmentsAndClickOnMutualFunds()
		.clickOnSearchMutualFunds()
		.selectAge()
		.selectMonthAndYear()
		.selectDay()
		.verifybday()
		.clickContinue()
		.enterSalary()
		.clickContinue()
		.selectBank()
		.enterName()
		.clickOnViewMutualFunds()
		.getAllSchemes();
	}

}
