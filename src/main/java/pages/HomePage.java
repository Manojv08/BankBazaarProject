package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	public MutualFundsPage mouseOverOnInvestmentsAndClickOnMutualFunds() {
		WebElement eleinvest = locateElement("linktext", "INVESTMENTS");
		Actions  builder = new Actions(driver);
		builder.moveToElement(eleinvest).pause(2000).perform();
		WebElement elemutual = locateElement("linktext", "Mutual Funds");
		click(elemutual);
		return new MutualFundsPage();
	}

}