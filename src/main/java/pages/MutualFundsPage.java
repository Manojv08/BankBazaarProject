package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import wdMethods.ProjectMethods;

public class MutualFundsPage extends ProjectMethods {
	
	WebDriverWait wait = new WebDriverWait(driver, 50);
	@CacheLookup
	@FindBy(linkText = "Search for Mutual Funds")
	WebElement elesrch;

	public MutualFundsPage() {
		PageFactory.initElements(driver, this);	
	}
	
	public MutualFundsPage clickOnSearchMutualFunds() throws InterruptedException {
		Thread.sleep(3000);
		WebElement elesrch = locateElement("linktext", "Search for Mutual Funds");
		click(elesrch);
		return this;
	}
	
	public MutualFundsPage selectAge() {
		wait.until(ExpectedConditions.elementToBeClickable(By.className("rangeslider__handle")));
		WebElement eleAgeBar = locateElement("class", "rangeslider__handle");
		click(eleAgeBar);
		return this;
	}
	
	public MutualFundsPage selectMonthAndYear() {
		WebElement elemonth = locateElement("linktext", "Aug 2000");
		click(elemonth);
		return this;
	}
	
	public MutualFundsPage selectDay() {
		WebElement eleday = locateElement("xpath", "//div[@class='react-datepicker__week']/following::div[text()='8']");
		click(eleday);
		return this;
	}
	
	public MutualFundsPage verifybday() {
		WebElement elebday = locateElement("xpath", "(//span[contains(@class , 'Calendar_highlight')])[3]");
		String bdaydate = getText(elebday);
		System.out.println(bdaydate);
		return this;
	}
	
	public MutualFundsPage clickContinue() {
		WebElement eleContinue = locateElement("linktext", "Continue");
		click(eleContinue);
		return this;
	}
	
	public MutualFundsPage enterSalary() {
		wait.until(ExpectedConditions.elementToBeClickable(By.name("netAnnualIncome")));
		WebElement eleSalary = locateElement("name", "netAnnualIncome");
		type(eleSalary, "550000");
		return this;
	}
	
	public MutualFundsPage selectBank() throws InterruptedException {
		Thread.sleep(3000);
//		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='HDFC']/following::input[@type='radio']")));
		WebElement elebank = locateElement("xpath", "//span[text()='HDFC']/following::input[@type='radio']");
		click(elebank);
		return this;
	}

	public MutualFundsPage enterName() {
		wait.until(ExpectedConditions.elementToBeClickable(By.name("firstName")));
		WebElement elefName = locateElement("name", "firstName");
		type(elefName, "Manoj");
		return this;
	}
	
	public FundSchemesPage clickOnViewMutualFunds() throws InterruptedException {
		WebElement eleViewmut = locateElement("linktext", "View Mutual Funds");
		click(eleViewmut);
		return new FundSchemesPage();
	}
}
