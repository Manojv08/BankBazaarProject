package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FundSchemesPage extends ProjectMethods{
	
	public FundSchemesPage() throws InterruptedException {
		Thread.sleep(5000);
		PageFactory.initElements(driver, this);
	}
	
	public FundSchemesPage getAllSchemes() {
		List<WebElement> allSchemenames = driver.findElementsByClassName("js-offer-name");
		for (WebElement eachSchemename : allSchemenames) {
			String schemename = getText(eachSchemename);
			reportStep("Scheme Name: " + schemename, "pass");
			System.out.println("Scheme Name: " + schemename);
			WebElement eleSchemeAmount = locateElement("xpath", "//span[contains(text(),'"+schemename+"')]/following::span[@class='fui-rupee bb-rupee-xs']/..");
			String eleAmount = getText(eleSchemeAmount);
			reportStep("Scheme Amount: " +eleAmount, "pass");
			System.out.println("Scheme Amount: " + eleAmount);
		}
		return this;
	}

}
